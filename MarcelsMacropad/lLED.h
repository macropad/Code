#ifndef ILED_H_
#define ILED_H_


#include <stdint.h>
#include <arduino.h>

class lLED
{
  public:
    lLED();

    void setGreen();
    void setBlue();
    void setRed();
    void setOrange();
    void setPurple();
    void setYellow();
    void setWhite();

    void setRGBB(int8_t red, int8_t green, int8_t blue, int8_t bright);

  private:
};

















#endif