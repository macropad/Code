

#include "Memory.h"
#include <Wire.h>
#include <Arduino.h>

Memory::Memory()
{
  addr = 0x51;
} 


uint8_t Memory::ReadMem(uint8_t* data, uint8_t index, uint8_t len)
{
  uint8_t counter = 0;
  Wire.beginTransmission(Memory::addr);
  Wire.write(index);
  Wire.endTransmission();

  delay(10);

  Wire.requestFrom(Memory::addr, len);

  delay(10);
  for (counter = 0; counter < len ; ++counter)
  {
    data[counter] = Wire.read();
  }
} 

uint8_t Memory::WriteMem(uint8_t* data, uint8_t index, uint8_t len)
{
  uint8_t Mask = 0x0F;
  uint8_t counter = 0;
  uint8_t inCounter = 0;
  uint8_t transferData[17] = {0};

  transferData[0] = index;
  
  for (inCounter = 0; inCounter < len; ++inCounter)
  {
    transferData[inCounter+1] = data[inCounter];
  }
  Wire.beginTransmission(addr);
  Wire.write(transferData,len+1);
  Wire.endTransmission();

  delay(10);
} 
