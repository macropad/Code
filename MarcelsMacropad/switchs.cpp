#include "switchs.h"

Switchs::Switchs()
{

}

void Switchs::registerKeyPressCallback(callbackFCN handler)
{
  KeypressHandler = handler;
}


void Switchs::action(int level, unsigned long time)
{
  switch (status)
  {
    case 0:
    {
      if (level == LvlHigh)
      {
        status++;
        lastEventUs = time;
        // Mach öpis
        KeypressHandler(1);
      }
      break;
    }
    case 1:
    {
      if ((time -lastEventUs) > 50000)
      {
        status++;
      }
      break;
    }
    case 2:
    {
      if (level == LvlLow)
      {
        status++;
        lastEventUs = time;
      }
      break;
    }
    case 3:
    {
      if ((time -lastEventUs) > 50000)
      {
        status = 0;
        // KeyreleaseHandler
        KeypressHandler(0);
      }
      break;
    }
    default:
    {
      status = 0;
    }
  }
}