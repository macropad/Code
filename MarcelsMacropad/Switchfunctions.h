#ifndef SWITCHFUNCTIONS_H_
#define SWITCHFUNCTIONS_H_

#include <stdint.h>
#include <string.h>
#include <Arduino.h>


void KBD_Setup(uint8_t lyr);

void sw01 (uint8_t status);

void sw02 (uint8_t status);

void sw03 (uint8_t status);

void sw04 (uint8_t status);

void sw05 (uint8_t status);

void sw06 (uint8_t status);

void sw07 (uint8_t status);

void sw08 (uint8_t status);

void sw09 (uint8_t status);

void sw10 (uint8_t status);

void sw11 (uint8_t status);

void sw12 (uint8_t status);

void sw13 (uint8_t status);

void sw14 (uint8_t status);

void sw15 (uint8_t status);

void rotEncChange (int8_t change);

uint8_t hasLayerChanged();

uint8_t hasColorChanged();

String getLayername();

uint8_t getColors(int8_t & r,int8_t & g,int8_t & b,int8_t & bright);

uint8_t getLayer();

String getWelcomeString();


#endif

