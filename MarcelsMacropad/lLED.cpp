#include "lLed.h"

enum RGBLed
{
  RGBRt = 10,
  RGBGn = 9,
  RGBBl = 11,
};

lLED::lLED()
{
  // RGB LED
  pinMode(RGBRt,OUTPUT);
  pinMode(RGBGn,OUTPUT);
  pinMode(RGBBl,OUTPUT);

  analogWrite(RGBRt,0);
  analogWrite(RGBGn,0);
  analogWrite(RGBBl,0);
}

void lLED::setGreen()
{
  analogWrite(RGBRt,0);
  analogWrite(RGBGn,50);
  analogWrite(RGBBl,0);
}

void lLED::setBlue()
{
  analogWrite(RGBRt,0);
  analogWrite(RGBGn,0);
  analogWrite(RGBBl,50);
}

void lLED::setRed()
{
  analogWrite(RGBRt,50);
  analogWrite(RGBGn,0);
  analogWrite(RGBBl,0);
} 
void lLED::setOrange()
{
  analogWrite(RGBRt,50);
  analogWrite(RGBGn,10);
  analogWrite(RGBBl,0);
}

void lLED::setPurple()
{
  analogWrite(RGBRt,50);
  analogWrite(RGBGn,0);
  analogWrite(RGBBl,50);
}

void lLED::setYellow()
{
  analogWrite(RGBRt,50);
  analogWrite(RGBGn,10);
  analogWrite(RGBBl,0);
}

void lLED::setWhite()
{
  analogWrite(RGBRt,50);
  analogWrite(RGBGn,50);
  analogWrite(RGBBl,50);  
}

void lLED::setRGBB(int8_t red, int8_t green, int8_t blue, int8_t bright)
{
  analogWrite(RGBRt,red*bright);
  analogWrite(RGBGn,green*bright);
  analogWrite(RGBBl,blue*bright);
}
