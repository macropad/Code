#include <string.h>
#include <stdint.h>
#include <Wire.h>

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#include <Encoder.h>

#include "switchs.h"
//#include "Customerspecific/AlAlSwitchFunctions.h"
#include "Switchfunctions.h"
#include "lLED.h"
#include "Memory.h"


// https://learn.adafruit.com/adafruit-gfx-graphics-library

enum screen
{
  SCREEN_WIDTH = 128,
  SCREEN_HEIGHT = 32,
  OLED_RESET = -1,
  SCREEN_ADDRESS = 0x3C,
};


void printDispLine(String text);

enum kbd
{
  nRows = 3,
  nCols = 5,
  nLayers = 3,
  nElemsPerLayer = 4,
  maxBrightness = 10,
  maxColor = 25,
};


const int rowsPin[nRows] = {0,1,4};
const int colsPin[nCols] = {23,5,13,19,20};

//#define DebugFlag

Switchs mSwitch[nRows][nCols];
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);
lLED mLed;
Memory mMemory;
Encoder myEnc(12, 6);

int8_t mColors [nLayers][nElemsPerLayer] = {};
uint8_t CurrentLayer = 0;

void setup() 
{
  uint8_t MemoryData[16]; 
  uint8_t curValue = 0;

#if defined (DebugFlag)
   Serial.begin(9600);
   while (!Serial) ; 
#endif
   
  Wire.begin();
  
  delay(200);

  // Keypad
  pinMode(rowsPin[0],OUTPUT);
  pinMode(rowsPin[1],OUTPUT);
  pinMode(rowsPin[2],OUTPUT);

  pinMode(colsPin[0],INPUT);
  pinMode(colsPin[1],INPUT);
  pinMode(colsPin[2],INPUT);
  pinMode(colsPin[3],INPUT);
  pinMode(colsPin[4],INPUT);

  mMemory.ReadMem(MemoryData,16,16);

  delay(100);
  
#if defined (DebugFlag)
  for (int i = 0; i<16; ++i) 
  {
    Serial.print(MemoryData[i]);
    Serial.write(" ");
  }
  Serial.write("\n");
#endif

  for (int i = 0; i<nLayers; ++i)
  {
    for (int j = 0; j<nElemsPerLayer; ++j)
    {
      curValue = MemoryData[i*nElemsPerLayer+j];
      if (j==3)
      {
        // index 0 = brighness --> Limit = 10;
        if (curValue > maxBrightness)
        {
          curValue = maxBrightness;
        }
      }
      else
      {
        // Other Indexes = Colors --> Limit = 25
        if (curValue > maxColor)
        {
          curValue = maxColor;
        }
      }
      mColors[i][j] = curValue;
    }
  }

  curValue = MemoryData[12];
  if (curValue > nLayers)
  {
    curValue = 0;
  }
  CurrentLayer = curValue;
  

  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) 
  {
    //TODO Error Mode Display
  }
  mLed.setBlue();
  display.clearDisplay();
  for(int16_t i=0; i<max(display.width(),display.height())/2; i+=2) 
  {
    display.drawCircle(display.width()/2, display.height()/8, i, SSD1306_WHITE);
    display.display();
    delay(1);
  }
  delay(100);
  printWelcomeMessage(getWelcomeString());

  delay(2000); // Pause for 2 seconds

  mSwitch[0][0].registerKeyPressCallback(sw15); //1
  mSwitch[0][1].registerKeyPressCallback(sw09); //2
  mSwitch[0][2].registerKeyPressCallback(sw10); //3
  mSwitch[0][3].registerKeyPressCallback(sw11); //4
  mSwitch[0][4].registerKeyPressCallback(sw12); //5
  mSwitch[1][0].registerKeyPressCallback(sw14); //6
  mSwitch[1][1].registerKeyPressCallback(sw05); //7
  mSwitch[1][2].registerKeyPressCallback(sw06); //8
  mSwitch[1][3].registerKeyPressCallback(sw07); //9
  mSwitch[1][4].registerKeyPressCallback(sw08); //10
  mSwitch[2][0].registerKeyPressCallback(sw13); //11
  mSwitch[2][1].registerKeyPressCallback(sw01); //12
  mSwitch[2][2].registerKeyPressCallback(sw02); //13
  mSwitch[2][3].registerKeyPressCallback(sw03); //14
  mSwitch[2][4].registerKeyPressCallback(sw04); //15

  
  display.clearDisplay();

  KBD_Setup(CurrentLayer);
  mLed.setRGBB(mColors[CurrentLayer][0],mColors[CurrentLayer][1],mColors[CurrentLayer][2],mColors[CurrentLayer][3]);
}

void loop() 
{
  static uint8_t state = 0;
  static unsigned long lastTime = micros();
  static unsigned long delay= 10;
  static uint8_t currentRow = 0; 

  static uint8_t hasToUpdateMemory = 0;
  static uint16_t memUpdateCounter = 0;
  static long lastMemTime = millis();
  const static uint16_t memUpdateLim = 5000;
  uint8_t MemoryData[16]; 


  uint8_t counter = 0;
  uint8_t pinState;

  long oldPosition=0;

  long newPosition = myEnc.read();
  if (newPosition != oldPosition) 
  {
    myEnc.write(0);
    oldPosition = newPosition;
    rotEncChange(newPosition);
  }

  if ((micros()- lastTime) > delay)
  {
    lastTime = micros();
    switch (state)
    {
      case 0: // Idle
      {
        state++;
        delay = 0;
        break;
      }
      case 1:
      {
        digitalWrite(rowsPin[currentRow], 1);
        delay = 1000;
        state = 2;
        break;
      }
      case 2:
      {
        for (counter = 0; counter < nCols; counter++)
        {
          pinState = digitalRead(colsPin[counter]);
          mSwitch[currentRow][counter].action(pinState,lastTime);
        }
        state = 3;
        delay = 500;
        break;
      }// case 2
      case 3:
      {
        digitalWrite(rowsPin[currentRow], 0);

        currentRow++;
        if(currentRow >= nRows)
        {
          currentRow = 0;
        }

        state = 1;
        delay = 2000;
        break;

      }// case 3
    }// Switch

    if (hasLayerChanged())
    {
      String txt = getLayername();
      CurrentLayer = getLayer();
      printDispLine(txt);
      mLed.setRGBB(mColors[CurrentLayer][0],mColors[CurrentLayer][1],mColors[CurrentLayer][2],mColors[CurrentLayer][3]);
      hasToUpdateMemory = 1;
      memUpdateCounter = 0;
    }

    if (hasColorChanged())
    {
      hasToUpdateMemory = 1; 
      memUpdateCounter = 0;

      int8_t redLvl = 0;
      int8_t grnLvl = 0;
      int8_t bluLvl = 0;
      int8_t brightness = 0;
      int8_t curValue;
      
      getColors(redLvl, grnLvl, bluLvl, brightness);
      mColors[CurrentLayer][0] += redLvl;
      mColors[CurrentLayer][1] += grnLvl;
      mColors[CurrentLayer][2] += bluLvl;
      mColors[CurrentLayer][3] += brightness;

      for (int j = 0; j<nElemsPerLayer; ++j)
      {
        curValue = mColors[CurrentLayer][j];
        if (j==3)
        {
          // index 0 = brighness --> Limit = 10;
          if (curValue > maxBrightness)
          {
            curValue = maxBrightness;
          }
          
        }
        else
        {
          // Other Indexes = Colors --> Limit = 25
          if (curValue > maxColor)
          {
            curValue = maxColor;
          }
        }
        if (curValue < 0)
        {
          curValue = 0;
        }
        mColors[CurrentLayer][j] = curValue;
      }


      
      mLed.setRGBB(mColors[CurrentLayer][0],mColors[CurrentLayer][1],mColors[CurrentLayer][2],mColors[CurrentLayer][3]);
#if defined (DebugFlag)
      Serial.print(mColors[CurrentLayer][0]);
      Serial.print(mColors[CurrentLayer][1]);
      Serial.print(mColors[CurrentLayer][2]);
      Serial.println(mColors[CurrentLayer][3]);
#endif

    } // if (hasColorChanged())
  }// If Time


  if (hasToUpdateMemory && ((millis()- lastMemTime) > memUpdateLim))
  {
    lastMemTime = millis();
    hasToUpdateMemory = 0;
    for (int i = 0; i<nLayers; ++i)
    {
      for (int j = 0; j<nElemsPerLayer; ++j)
      {
        MemoryData[i*nElemsPerLayer+j]  = mColors[i][j];
      }
    }
    MemoryData[12] = CurrentLayer;

    mMemory.WriteMem(MemoryData,16,16);

#if defined (DebugFlag)
    for (int i = 0; i<16; ++i) 
    {
      if (i%4 == 0)
      {
        Serial.write("   ");
      }
      Serial.print(MemoryData[i]);
      Serial.write(" ");
      
    }
    Serial.write("\n");
  
#endif
  } // hasToUpdateMemory && ((millis()- lastMemTime) > memUpdateLim)
 
  
}// Loop

void printDispLine(String text)
{
  display.clearDisplay();
  display.setTextSize(2); // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(1, 13);
  display.println(text);
  display.display();      // Show initial text
}

void printWelcomeMessage(String text)
{
  display.clearDisplay();
  display.setTextSize(1); // Draw 2X-scale text
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(1, 13);
  display.println(text);
  display.display();      // Show initial text
}

