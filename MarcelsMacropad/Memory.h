#ifndef MEMORY_H_
#define MEMORY_H_ 

#include <stdint.h>

class Memory
{
  public:
    Memory();
    uint8_t ReadMem(uint8_t* data, uint8_t index, uint8_t len);
    uint8_t WriteMem(uint8_t* data, uint8_t index, uint8_t len);
  private: 
    uint8_t addr;
};

#endif