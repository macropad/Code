#include "Switchfunctions.h"
//#include <Keyboard.h>

#define KEYDELAY 2


#define HID_CUSTOM_LAYOUT // set this flag to indicate that a custom layout is selected
// if the above flag is not defined, then the default US layout is used instead

#define LAYOUT_GERMAN_SWISS // set this flag after the above flag to indicate the custom input method is German
// for more layouts, see /src/KeyboardLayouts/ImprovedKeylayouts.h
//#include "HID-Project.h"
#include <HID-Project.h>
// use this option for OSX:
//char ctrlKey = KEY_LEFT_GUI;
// use this option for Windows and Linux:
char ctrlKey = KEY_LEFT_CTRL;

#include <Arduino.h>

typedef enum lyrs
{
  Lay1 = 0,
  Lay2,
  Lay3,
} KBDLName;

enum
{
  nLayers = 3,
};

String LayerName[nLayers] = {"NumPad", "Alt-Shft-F", "Teams"};

String getWelcomeString()
{
  String msg = "Hello UserMMMMMMMMM";
  return (msg);
}

static KBDLName layer = Lay1;

uint8_t ColorChangeMode = 0;

static uint8_t LayerChanged = 1;

uint8_t ColorChanged = 1;

int8_t redLvl = 0;
int8_t grnLvl = 0;
int8_t bluLvl = 0; 
int8_t brightness = 0; 


void KBD_Setup(uint8_t lyr)
{
  Consumer.begin();
  Keyboard.begin();

  layer = lyr;
}

/*************************************************************************/
void sw01 (uint8_t status)
{
  if (status)
  {
    switch(layer)
    {
      case Lay1:
      {
        Keyboard.press(KEYPAD_1);
        //delay(KEYDELAY);
        //Keyboard.releaseAll();
        break;
      }
      case Lay2:
      {
        Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
        Keyboard.press(HID_KEYBOARD_LEFT_ALT);
        Keyboard.press(KEY_F1);   
        break;
      }
      case Lay3:
      {
        break;
      }
      default:
      {
        break;
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
void sw02 (uint8_t status)
{
  if (status)
  {
    switch(layer)
    {
      case Lay1:
      {
        Keyboard.press(KEYPAD_2);
        break;
      }
      case Lay2:
      {
        Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
        Keyboard.press(HID_KEYBOARD_LEFT_ALT);
        Keyboard.press(KEY_F2);   
        break;
      }
      case Lay3:
      {
        break;
      }
      default:
      {
        break;
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
void sw03 (uint8_t status)
{
  if (status)
  {
    switch(layer)
    {
      case Lay1:
      {        
        Keyboard.press(KEYPAD_3);
        break;
      }
      case Lay2:
      {
        Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
        Keyboard.press(HID_KEYBOARD_LEFT_ALT);
        Keyboard.press(KEY_F3);   
        break;
      }
      case Lay3:
      {
        break;
      }
      default:
      {
        break;
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}


/*************************************************************************/
void sw04 (uint8_t status)
{
  if (status)
  {
    switch(layer)
    {
      case Lay1:
      {        
        Keyboard.press(KEYPAD_4);
        break;
      }
      case Lay2:
      {
        Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
        Keyboard.press(HID_KEYBOARD_LEFT_ALT);
        Keyboard.press(KEY_F4);   
        break;
      }
      case Lay3:
      {
        break;
      }
      default:
      {
        break;
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
void sw05 (uint8_t status)
{
  if (status)
  {
    if (ColorChangeMode)
    {
      ColorChanged = 1;
      redLvl = 0;
      grnLvl = 0;
      bluLvl = 0;
      brightness   = 1;
    }
    else
    {
      switch(layer)
      {
        case Lay1:
        {
          Keyboard.press(KEYPAD_5);
          break;
        }
        case Lay2:
        {
          Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
          Keyboard.press(HID_KEYBOARD_LEFT_ALT);
          Keyboard.press(KEY_F5);   
          break;
        }
        case Lay3:
        {
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
void sw06 (uint8_t status)
{
  if (status)
  {
    if (ColorChangeMode)
    {
      ColorChanged = 1;
      redLvl = 1;
      grnLvl = 0;
      bluLvl = 0;
      brightness   = 0;
    }
    else
    {
      switch(layer)
      {
        case Lay1:
        {
          Keyboard.press(KEYPAD_6);
          break;
        }
        case Lay2:
        {
          Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
          Keyboard.press(HID_KEYBOARD_LEFT_ALT);
          Keyboard.press(KEY_F6);   
          break;
        }
        case Lay3:
        {
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
void sw07 (uint8_t status)
{
  if (status)
  {
    if (ColorChangeMode)
    {
      ColorChanged = 1;
      redLvl = 0;
      grnLvl = 1;
      bluLvl = 0;
      brightness   = 0;
    }
    else
    {
      switch(layer)
      {
        case Lay1:
        {
          Keyboard.press(KEYPAD_7);
          break;
        }
        case Lay2:
        {
          Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
          Keyboard.press(HID_KEYBOARD_LEFT_ALT);
          Keyboard.press(KEY_F7);   
          break;
        }
        case Lay3:
        {
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}
/*************************************************************************/
void sw08 (uint8_t status)
{
  if (status)
  { 
    if (ColorChangeMode)
    {
      ColorChanged = 1;
      redLvl = 0;
      grnLvl = 0;
      bluLvl = 1;
      brightness   = 0;
    }
    else
    {
      switch(layer)
      {
        case Lay1:
        {
          Keyboard.press(KEYPAD_8);
          break;
        }
        case Lay2:
        {
          Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
          Keyboard.press(HID_KEYBOARD_LEFT_ALT);
          Keyboard.press(KEY_F8);   
          break;
        }
        case Lay3:
        {
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}
/*************************************************************************/
void sw09 (uint8_t status)
{
  if (status)
  {
    if (ColorChangeMode)
    {
      ColorChanged = 1;
      redLvl = 0;
      grnLvl = 0;
      bluLvl = 0;
      brightness   = -1;
    }
    else
    {
      switch(layer)
      {
        case Lay1:
        {
          Keyboard.press(KEYPAD_9);
          break;
        }
        case Lay2:
        {
          Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
          Keyboard.press(HID_KEYBOARD_LEFT_ALT);
          Keyboard.press(KEY_F9);   
          break;
        }
        case Lay3:
        {
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
void sw10 (uint8_t status)
{
  if (status)
  {
    if (ColorChangeMode)
    {
      ColorChanged = 1;
      redLvl = -1;
      grnLvl = 0;
      bluLvl = 0;
      brightness = 0;
    }
    else
    {
      switch(layer)
      {
        case Lay1:
        {
          Keyboard.press(KEYPAD_0);
          break;
        }
        case Lay2:
        {
          Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
          Keyboard.press(HID_KEYBOARD_LEFT_ALT);
          Keyboard.press(KEY_F10);   
          break;
        }
        case Lay3:
        {
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
void sw11 (uint8_t status)
{
  if (status)
  {
    if (ColorChangeMode)
    {
      ColorChanged = 1;
      redLvl = 0;
      grnLvl = -1;
      bluLvl = 0;
      brightness = 0;
    }
    else
    {
      switch(layer)
      {
        case Lay1:
        {
          Keyboard.press(KEYPAD_DOT);
          break;
        }
        case Lay2:
        {
          Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
          Keyboard.press(HID_KEYBOARD_LEFT_ALT);
          Keyboard.press(KEY_F11);   
          break;
        }
        case Lay3:
        {
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
void sw12 (uint8_t status)
{
  if (status)
  {
    if (ColorChangeMode)
    {
      ColorChanged = 1;
      redLvl = 0;
      grnLvl = 0;
      bluLvl = -1;
      brightness = 0;
    }
    else
    {
      switch(layer)
      {
        case Lay1:
        {
          Keyboard.press(KEYPAD_ENTER);
          break;
        }
        case Lay2:
        {
          Keyboard.press(HID_KEYBOARD_LEFT_SHIFT);
          Keyboard.press(HID_KEYBOARD_LEFT_ALT);
          Keyboard.press(KEY_F12);   
          break;
        }
        case Lay3:
        {
          break;
        }
        default:
        {
          break;
        }
      }
    }
  }
  else
  {
    Keyboard.releaseAll();
  }  
}

/*************************************************************************/
// LED Brightness --> TODO
void sw13 (uint8_t status)
{
  if (status)
  {
    ColorChangeMode = 1;
  }
  else
  {
    ColorChangeMode = 0;
  }  
}

/*************************************************************************/
// Layer Switch
void sw14 (uint8_t status)
{
  if (status)
  {
    switch(layer)
    {
      case Lay1:
      {
        layer = Lay2;
        LayerChanged = 1;
        break;
      }
      case Lay2:
      {
        layer = Lay3;
        LayerChanged = 1;
        break;
      }
      case Lay3:
      {
        layer = Lay1;
        LayerChanged = 1;
        break;
      }
      default:
      {
        layer = Lay1;
        LayerChanged = 1;
        break;
      }
    }
  }
  
}

/*************************************************************************/
void sw15 (uint8_t status)
{ 
  if (status)
  {
    switch(layer)
    {
      case Lay1:
      {
        
        break;
      }
      case Lay2:
      {
        break;
      }
      case Lay3:
      {
        Consumer.write(MEDIA_PLAY_PAUSE);
        break;
      }
      default:
      {
        break;
      }
    }
  }
  else
  {
    //Keyboard.releaseAll();
  }  
}


void rotEncChange (int8_t change)
{
  switch(layer)
    {
      case Lay1:
      {
        
        break;
      }
      case Lay2:
      {
        break;
      }
      case Lay3:
      {
        if (change < 0)
        {
            Consumer.write(MEDIA_VOL_DOWN);
            delay(KEYDELAY);
        }
        else
        {
            Consumer.write(MEDIA_VOL_UP);
            delay(KEYDELAY);
        }
        break;
      }
      default:
      {
        break;
      }
    }
  
}

uint8_t hasLayerChanged()
{
  uint8_t temp = LayerChanged;
  LayerChanged = 0;
  return temp;
}

uint8_t hasColorChanged()
{
  uint8_t temp = ColorChanged;
  ColorChanged = 0;
  return temp;
}

String getLayername()
{
  return LayerName[layer];
}

uint8_t getColors(int8_t & r,int8_t & g,int8_t & b,int8_t & bright)
{
  bright = brightness;
  r = redLvl;
  g = grnLvl;
  b = bluLvl; 
  return 0;
}

uint8_t getLayer()
{
  return layer;
}