#ifndef SWITCH_H_
#define SWITCH_H_


#include <stdint.h>

enum
{
  LvlHigh = 1,
  LvlLow = 0,
};

typedef void (*callbackFCN)(uint8_t status);

class Switchs
{
  public:
    Switchs();

    void registerKeyPressCallback(callbackFCN handler);
    void action(int level, unsigned long time);

  private:
    callbackFCN  KeypressHandler;
    unsigned long lastEventUs = 0;
    uint8_t oldLevel = 0;
    uint8_t status = 0;
};
















#endif