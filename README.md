# Code



## Intro
This contains the code for the Macropad as you've maybe got one in January 2024. 

## Usage
If you decide to change the keys configurations do that in the Switchfunctions.cpp

The switches are numbered in reading order for the main 12 Buttons. The Rotary Encoder's Button is Nr 15. The Button to the left of the main Block is Nr 14 and there to change the layers.
Nr 13 is hidden on the top left corner. With this button you can change the LED's color. The first columns lower button are for the brightness, the second column is for red, the third is for green and the last for blue.

Currently there are three layers implemented. This is also the maximum for the moment. 